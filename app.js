var express = require('express');

var app = express();

app.get('/', function (req, res) {
  res.send('hello world');
});

app.get('/p2', function (req, res) {
  res.send('hello world x page 2');
});


app.listen(process.env.PORT || 5000);

module.exports = app;
